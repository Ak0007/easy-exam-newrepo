// const express = require('express')
// const router = express.Router();
// // const all_boards=require('../public/all-boards.ejs')
//
// router.get("/", function (req, res) {
//     res.render('index');
// });
//
// router.get("/all-boards", function (req, res) {
//     res.render('all-boards');
// })
//
// module.exports = router


module.exports = [
    {
        route: "/",
        render: "index"
    },
    {
        route: "/all-boards",
        render: "all-boards"
    },
    {
        route: "/all-class",
        render: "all-class"
    },
    {
        route: "/about-student",
        render: "about-student"
    },
    {
        route: "/add-student",
        render: "add-student"
    },
    {
        route: "/about-teacher",
        render: "about-teacher"
    },
    {
        route: "/add-teacher",
        render: "add-teacher"
    },
    {
        route: "/about-tutorial",
        render: "about-tutorial"
    },
    {
        route: "/add-tutorial",
        render: "add-tutorial"
    },
    {
        route: "/add-mcq-assessment",
        render: "add-mcq-assessment"
    },
    {
        route: "/add-assessment",
        render: "add-descriptive-assessment"
    },
    {
        route: "/add-session",
        render: "add-session"
    },
    {
        route: "/add-lesson",
        render: "add-lession"
    },
	{
        route: "/add-tutorial-quiz",
        render: "add-tutorial-quiz"
    },
{
        route: "/add-mcq-quiz",
        render: "add-mcq-quiz"
    },

    {
        route: "/add-topic",
        render: "add-topic"
    },
    
	{
        route: "/all-assessment",
        render: "all-assessment"
    },
    {
        route: "/all-board-paper",
        render: "all-board-paper"
    },
    {
        route: "/add-board-paper",
        render: "add-board-paper"
    },
    {
        route: "/all-lessons",
        render: "all-lessons"
    },
    {
        route: "/all-session",
        render: "all-session"
    },
    {
        route: "/all-students",
        render: "all-students"
    },
    {
        route: "/all-subjects",
        render: "all-subjects"
    },
    {
        route: "/all-teachers",
        render: "all-teachers"
    },
    {
        route: "/add-textbook",
        render: "add-textbook"
    },
    {
        route: "/all-textbook",
        render: "all-textbook"
    },
    {
        route: "/all-topics",
        render: "all-topics"
    },
    {
        route: "/all-tutorials",
        render: "all-tutorials"
    },
    {
        route: "/all-uploadreq",
        render: "all-uploadreq"
    },
    {
        route: "/page-forgot-password",
        render: "page-forgot-password"
    },
    {
        route: "/page-lock-screen",
        render: "page-lock-screen"
    },
    {
        route: "/page-login",
        render: "page-login"
    },
]
