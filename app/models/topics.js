const mongoose = require('mongoose'),
    Question = require('./Question.js'),
    QuestionSchema = mongoose.model('Question').schema;


const Schema = mongoose.Schema;


const topicSchema = new Schema({
    // lesson id
    lesson: {
        type:mongoose.Schema.Types.ObjectId,
        ref:'lessons'
    },
    // topic name
    topicName: {
        type: String
    },
    // topic description
    description: {
        type: String
    },
    // video link
    video: {
        type: String
    },
    // material pdf link
    readingMaterial: [{
        type: String
    }],
    // home work
    worksheet: {
        type: String
    },
    quiz: [QuestionSchema],
    created: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('topics', topicSchema);
