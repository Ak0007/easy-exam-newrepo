const mongoose = require('mongoose')

const Schema  =  mongoose.Schema;


const discriptiveTestSchema = new Schema({

    title: {
        type: String
    },
    questionPaper: {
        type: String
    },
    answersheet: {
        type: String
    },

    createdBy: {
        type:mongoose.Schema.Types.ObjectId,
        ref:'User'
    },
    created: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('DiscriptiveTest', discriptiveTestSchema);
