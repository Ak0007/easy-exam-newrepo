const mongoose = require('mongoose'),
    Question = require('./Question.js'),
    QuestionSchema = mongoose.model('Question').schema;


const Schema = mongoose.Schema;


const ClassesSchema = new Schema({
    className: {
        type: String
    },
    createdAt: {
        type: Date
    },
    samplePapers: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "SamplePaper"
        }
    ],
    boardPapers: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "BoardPaper"
        }
    ],
});

module.exports = mongoose.model('classes', ClassesSchema);
