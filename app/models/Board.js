const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const BoardSchema = new Schema({
    boardName: {
        type: String
    },
    description: {
        type: String
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    createdAt: {
        type: Date
    }
});

module.exports = mongoose.model('boards', BoardSchema);
