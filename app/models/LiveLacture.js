const mongoose = require('mongoose'),
    Topic = require('./Topic.js'),
    TopicSchema = mongoose.model('Topic').schema;


const Schema = mongoose.Schema;


const liveLactureSchema = new Schema({
    lessonName: {
        type: String
    },
    subject: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Subject"
        }
    ],
    
    teacher: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User"
        }
    ],
    duscriptiveTests: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "DiscriptiveTest"
        }
    ],
    addeddBy: {
        type: mongoose.Schema.Types.ObjectId,
            ref: "User"
    },
    ScheduledTime: {
        type: Date
    },
    studentLimit: {
        type: Number
    },
    thumbnail: {
        type: String
    },
    status: {
        type: String
    },
    isRecordingAllowed: {
        type: Boolean
    },
    RafranceMaterial: [{
        type: String
    }],
    preSessionDocuments: [{
        type: String
    }],

    createdAt: {
        type: Date
    }
});

module.exports = mongoose.model('LiveLacture', liveLactureSchema);
