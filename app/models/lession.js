const mongoose = require('mongoose')
const TopicSchema = require('./topics')


const Schema = mongoose.Schema;


const lessonSchema =new mongoose.Schema({
    lessonName: {
        type: String
    },
    topics: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Topic"
        }
    ],

    tests: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Test"
        }
    ],
    duscriptiveTests: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "DiscriptiveTest"
        }
    ],
	isDeleted: {
        type: Boolean,
        default: false
    },

    createdBy: {
        type: String
    },
    createdAt: {
        type: Date
    }
});

module.exports = mongoose.model('lessons', lessonSchema);
