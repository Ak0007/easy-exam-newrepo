const mongoose = require('mongoose');

const Schema  =  mongoose.Schema;


const boardPaperSchema = new Schema({
    title: {
        type: String
    },
    questionPaper: {
        type: String
    },
    answers: {
        type: String
    },
    adedBy: {
        type:mongoose.Schema.Types.ObjectId,
        ref:'User'
    },
    created: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('BoardPaper', boardPaperSchema);
