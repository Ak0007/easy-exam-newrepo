const mongoose = require('mongoose')
     // Setting = require('./Question.js'),
     // SettingSchema = mongoose.model('Setting').schema;

const Schema  =  mongoose.Schema;


const userSettingSchema = new Schema({

    user: {
        type:mongoose.Schema.Types.ObjectId,
        ref:'User'
    },
    // settings: [SettingSchema],
    created: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Test', userSettingSchema);
