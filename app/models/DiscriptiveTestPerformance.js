const mongoose = require('mongoose')


const Schema  =  mongoose.Schema;


const discriptiveTestPerformaceSchema = new Schema({


    answersheet: {
        type: String
    },
    scheduledTime: {
        type: Date,
    },
    Attempted: {
        type: Boolean,
        default: false
    },
    timeTaken: {
        type: Number,
        default: 0
    },
    score: {
        type: String
    },
    user: {
        type:mongoose.Schema.Types.ObjectId,
        ref:'User'
    },
    test: {
        type:mongoose.Schema.Types.ObjectId,
        ref:'DiscriptiveTest'
    },
    evaluatedBy: {
        type:mongoose.Schema.Types.ObjectId,
        ref:'User'
    },
    created: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('DiscriptiveTestPerformance', discriptiveTestPerformaceSchema);
