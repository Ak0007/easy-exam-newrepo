const mongoose = require('mongoose'),
    Schema = mongoose.Schema;


const samplePaperSchema = new Schema({

    title: {
        type: String
    },
    questionPaper: {
        type: String
    },
    answers: {
        type: String
    },
    addedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    evaluatedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    created: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('SamplePaper', samplePaperSchema);
