const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const subjectSchema = new Schema({
    subjectName: {
        type: String
    },
    class: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'classes'
    },
    board: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'boards'
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Test', subjectSchema);
