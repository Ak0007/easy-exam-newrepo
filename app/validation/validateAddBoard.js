const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateLogin(data) {
    let errors = {};

    data.boardName = !isEmpty(data.boardName) ? data.boardName : "";


    if (Validator.isEmpty(data.boardName)) {
        errors.boardName = "Board Name is required";
    }

    return {
        errors,
        isValid: isEmpty(errors)
    }
};
