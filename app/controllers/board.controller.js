const mongoose = require('mongoose');
const express = require('express');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const events = require('events');
const nodemailer = require('nodemailer');
const passport = require('passport');

const userRouter = express.Router();
const boardRouter = express.Router();

const userModel = require('../models/User')
const socialModel = require('../models/SocialUser')
const testModel = require('../models/Test')
const Board = require('../models/Board')
const performanceModel = require('../models/Performance')

//libraries and middlewares
const config = require('./../../config/config.js');
const responseGenerator = require('./../../libs/responseGenerator');
const auth = require("./../../middlewares/auth");
const random = require("randomstring");
// validations
const isEmpty = require("../validation/is-empty")
const validateAddBoard = require("../validation/validateAddBoard")

// *********** ALL API'S ********************//


module.exports.controller = (app) => {

    boardRouter.post('/addboard', async (req, res) => {
        try {
            const {errors, isValid} = validateAddBoard(req.body)
            if (!isValid) {
                res.json(responseGenerator.generate(true, errors, 400, null));
            } else {
                const data = {}
                if (req.body.boardName) data.boardName = req.body.boardName
                if (req.body.description) data.description = req.body.description
                data.createdAt = Date.now()

                const addData = await Board(data).save()

                res.json(responseGenerator.generate(false, null, 200, addData));
            }
        } catch (err) {
            console.log(err)
            res.send(responseGenerator.generate(true, "Some Internal Error", 500, null));
        }
    })

    boardRouter.get('/list', async (req, res) => {
        try {
            const board = await Board.find({isDeleted: false})
            if (isEmpty(board)) {
                res.json(responseGenerator.generate(false, "No board found", 404, null));
            } else {
                res.json(responseGenerator.generate(false, null, 200, board));
            }
        } catch (err) {
            console.log(err)
            res.send(responseGenerator.generate(true, "Some Internal Error", 500, null));
        }
    })


    boardRouter.get('/:boardId', async (req, res) => {
        try {
            const board = await Board.findOne({_id: req.params.boardId, isDeleted: false})
            if (isEmpty(board)) {
                res.json(responseGenerator.generate(false, "No board found", 404, null));
            } else {
                res.json(responseGenerator.generate(false, null, 200, board));
            }
        } catch (err) {
            console.log(err)
            res.send(responseGenerator.generate(true, "Some Internal Error", 500, null));
        }
    })

    boardRouter.put('/update/:boardId', async (req, res) => {
        try {
            const {errors, isValid} = validateAddBoard(req.body)
            if (!isValid) {
                res.json(responseGenerator.generate(true, errors, 400, null));
            } else {

                const boardExist = await Board.findOne({_id: req.params.boardId, isDeleted: false})
                if (!boardExist) {
                    res.json(responseGenerator.generate(false, "No board found", 404, null));
                } else {
                    const data = {}
                    if (req.body.boardName) data.boardName = req.body.boardName
                    if (req.body.description) data.description = req.body.description

                    const update = await Board.findOneAndUpdate({_id: req.params.boardId}, {$set: data}, {new: true});

                    res.json(responseGenerator.generate(false, null, 200, update));
                }
            }
        } catch (err) {
            console.log(err)
            res.send(responseGenerator.generate(true, "Some Internal Error", 500, null));
        }
    })

    boardRouter.delete('/delete/:boardId', async (req, res) => {
        try {
            const boardExist = await Board.findOne({_id: req.params.boardId, isDeleted: false})
            if (!boardExist) {
                res.json(responseGenerator.generate(false, "No board found", 404, null));
            } else {

                const update = await Board.findOneAndUpdate({_id: req.params.boardId}, {isDeleted: true}, {new: true});

                res.json(responseGenerator.generate(false, null, 200, null));
            }
        } catch
            (err) {
            console.log(err)
            res.send(responseGenerator.generate(true, "Some Internal Error", 500, null));
        }
    });

// API to edit question
    /*boardRouter.put('/test/editQuestion/:qid',  (req, res) =>{
        testModel.findOneAndUpdate({
            "questions._id": req.params.qid
        }, {
            "$set": {
                "questions.$.question": req.body.question,
                "questions.$.optionA": req.body.optionA,
                "questions.$.optionB": req.body.optionB,
                "questions.$.optionC": req.body.optionC,
                "questions.$.optionD": req.body.optionD,
                "questions.$.answer": req.body.answer
            }
        },  (err) =>{
            if (err) {
                let response = responseGenerator.generate(true, "Some Internal Error", 500, null);
                res.send(response);
            } else {
                let response = responseGenerator.generate(false, "Question Edited Successfully", 200, null);
                res.send(response);
            }
        });
    });


    // API to edit test details
    boardRouter.put('/test/edit/:tid',  (req, res) =>{

        testModel.findOneAndUpdate({
            "testid": req.params.tid
        }, req.body,  (err,test) =>{
            if (err) {
                let response = responseGenerator.generate(true, "Some Internal Error", 500, null);
                res.send(response);
            } else {
                let response = responseGenerator.generate(false, "Test Edited Successfully", 200, null);
                res.send(response);
            }
        });
    });


    //to get all questions by User as well as Admin
    boardRouter.get('/test/:tid/getQuestions',  (req, res)=> {
        testModel.find({
            testid: req.params.tid
        },  (err, test)=> {
            if (err) {
                let error = responseGenerator.generate(true, "Something is not working, error : " + err, 500, null);
                res.send(error);
            } else if (test === null || test === undefined || test === []) {
                let error = responseGenerator.generate(false, "No Question added in this test!", 204, null);
                res.send(error);
            } else {
                let response = responseGenerator.generate(false, "All Questions fetched successfully", 200, test[0].questions);
                res.send(response);
            }
        })
    });

    // api to store test attempted 	by users
    boardRouter.post('/tests/:tid/attemptedby',  (req, res) =>{
        var data={
            email:req.body.email,
            score:req.body.score
        }
        testModel.findOneAndUpdate({
            'testid': req.params.tid
        }, {
            '$push': {
                testAttemptedBy: data
            }
        }, (err)=> {
            if (err) {
                let response = responseGenerator.generate(true, "Some Error Ocurred, error : " + err, 500, null);
                res.send(response);
            } else {
                let response = responseGenerator.generate(false, "Successfully Updated The Test", 200, null);
                res.send(response);
            }
        });
    });


    //API to get tests attempted by a user
    boardRouter.get('/usertests/:tid',  (req, res) =>{

        testModel.find({
            testid: req.params.tid
        },  (err, result)=> {
            if (err) {
                response = responseGenerator.generate(true, "Some Internal Error", 500, null);
                res.send(response);
            } else if (result === null || result === undefined || result === []) {
                let error = responseGenerator.generate(false, "No users attempted the test!", 204, null);
                res.send(error);
            }
            else {
                response = responseGenerator.generate(false, "Tests Taken By User", 200, result.testAttemptedBy);
                res.send(response);
            }
        });
    });

    //API to get the performances of all users
    boardRouter.get('/all/performances',  (req, res) =>{
        //api to get  performance user specific
    performanceModel.find({},  (err, Performances)=> {
        if (err) {
            let error = responseGenerator.generate(true, "Something is not working, error : " + err, 500, null);
            res.send(error);

        } else {
            let response = responseGenerator.generate(false, "Performances fetched successfully!!!", 200, Performances);
            res.send(response);
        }
    });
    });

    //API to get the performance of all users in a particular test

    boardRouter.get('/all/performance/:tid',  (req, res) =>{
        //api to get  performance user specific
    performanceModel.find({
            testId:req.params.tid
    },  (err, Performance)=> {
        if (err) {
            let error = responseGenerator.generate(true, "Something is not working, error : " + err, 500, null);
            res.send(error);

        } else {
            let response = responseGenerator.generate(false, "TotalPerformance of user in all Tests fetched successfully!!!", 200, Performance);
            res.send(response);
        }
    });
    });


    //API to get the performance of a single user in a particular test

    boardRouter.get('/performance/:tid',  (req, res) =>{
        //api to get  performance user specific
    performanceModel.findOne({
           $and:[{userEmail: req.user.email} , {testId:req.params.tid}]
    },  (err, Performance) =>{
        if (err) {
            let error = responseGenerator.generate(true, "Something is not working, error : " + err, 500, null);
            res.send(error);

        } else {
            let response = responseGenerator.generate(false, "TotalPerformance of user in all Tests fetched successfully!!!", 200, Performance);
            res.send(response);
        }
    });
    });


    //API to add the performance of the user
    boardRouter.post('/addPerformance',  (req, res) =>{

    let performance = new performanceModel({
    userEmail: req.body.email,
    testId: req.body.testid,
    sc                                                                  ore: req.body.score,
    questionCount: req.body.noOfQuestions,
    timeTaken:req.body.timetaken,
    totalCorrectAnswers:req.body.totalCorrectAnswers,
    totalSkipped:req.body.totalSkipped
    });
    //console.log(taker);
    performance.save( (err, result) =>{
    if (err) {
        response = responseGenerator.generate(true, "Some Internal Error", 500, null);
        res.send(response);
    } else {
        response = responseGenerator.generate(false, "Added Test Performance Successfully", 200, performance);
        res.send(response);
    }
    });
    });*/


// app.use('/user', auth.verifyToken, boardRouter);
    app.use('/board', boardRouter);


}


