const express = require('express');
const router = express.Router();
const passport = require('passport')
//Models
const Topics = require('../models/topics')
const Lesson = require('../models/lession')
//libraries and middlewares
const responseGenerator = require('./../../libs/responseGenerator');
const isEmpty = require("../../libs/is-empty");

module.exports.controller = (app) => {
    router.post('/add', passport.authenticate('jwt', {session: false}), async (req, res, next) => {
        try {
            const lessonExist = await Lesson.exists({_id: req.body.lesson})
            if (!lessonExist) {
                res.send(responseGenerator.generate(true, "Lesson does not exists", 404, null))
            }
            const data = {}
            if (req.body.lesson) data.lesson = req.body.lesson
            if (req.body.topicName) data.topicName = req.body.topicName
            if (req.body.description) data.description = req.body.description
            if (req.body.video) data.video = req.body.video
            if (req.body.readingMaterial) data.readingMaterial = req.body.readingMaterial
            if (req.body.worksheet) data.worksheet = req.body.worksheet
            if (req.body.quiz) data.quiz = req.body.quiz

            const newTopic = await new Topics(data).save()
            res.send(responseGenerator.generate(false, "Topic added successful!!", 200, newTopic))
        } catch (err) {
            console.log(err)
            res.send(responseGenerator.generate(false, "Internal server error", 500, null))
        }
    })

    router.get('/list', passport.authenticate('jwt', {session: false}), async (req, res, next) => {
        try {
            const topics = await Topics.find().populate('lesson')

            if (isEmpty(topics)) {
                res.send(responseGenerator.generate(true, "No record found!!", 404, null))
            } else {
                res.send(responseGenerator.generate(true, "Request successful!!", 200, topics))
            }
        } catch (err) {
            console.log(err)
            res.send(responseGenerator.generate(false, "Internal server error", 500, null))
        }
    })

    router.get('/topic/:topicId', passport.authenticate('jwt', {session: false}), async (req, res, next) => {
        try {
            const topics = await Topics.findOne({_id: req.params.topicId}).populate('lesson')

            if (isEmpty(topics)) {
                res.send(responseGenerator.generate(true, "No record found!!", 404, null))
            } else {
                res.send(responseGenerator.generate(true, "Request successful!!", 200, topics))
            }
        } catch (err) {
            console.log(err)
            res.send(responseGenerator.generate(false, "Internal server error", 500, null))
        }
    })

    router.put('/update/:topicId', passport.authenticate('jwt', {session: false}), async (req, res, next) => {
        try {
            const topics = await Topics.findOne({_id: req.params.topicId})

            if (isEmpty(topics)) {
                res.send(responseGenerator.generate(true, "No record found!!", 404, null))
            } else {
                const data = {}
                if (req.body.lesson) data.lesson = req.body.lesson
                if (req.body.topicName) data.topicName = req.body.topicName
                if (req.body.description) data.description = req.body.description
                if (req.body.video) data.video = req.body.video
                if (req.body.readingMaterial) data.readingMaterial = req.body.readingMaterial
                if (req.body.worksheet) data.worksheet = req.body.worksheet
                if (req.body.quiz) data.quiz = req.body.quiz

                const updateTopic = await Topics.findOneAndUpdate({_id: req.params.topicId}, {$set: data}, {new: true})

                res.send(responseGenerator.generate(true, "Topic updated successful!!", 200, updateTopic))
            }
        } catch (err) {
            console.log(err)
            res.send(responseGenerator.generate(false, "Internal server error", 500, null))
        }
    })

    router.delete('/delete/:topicId', passport.authenticate('jwt', {session: false}), async (req, res, next) => {
        try {
            const topics = await Topics.findOne({_id: req.params.topicId})

            if (isEmpty(topics)) {
                res.send(responseGenerator.generate(true, "No record found!!", 404, null))
            } else {
                const removeTopic = await Topics.findOneAndRemove({_id: req.params.topicId})
                res.send(responseGenerator.generate(true, "Topic removed successful!!", 200, removeTopic))
            }
        } catch (err) {
            console.log(err)
            res.send(responseGenerator.generate(false, "Internal server error", 500, null))
        }
    })
    app.use('/topics', router);
}
